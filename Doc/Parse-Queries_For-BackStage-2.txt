APPID: fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x
JSKEY: qdEVaFuYe08mtL81o45DhhS4wYZHlnKbLrTk2nAM
RESTAPIKEY: vKZV2i5IKhs96Ph5lLJLCBKPtaAMGxsfIoqPduhh

https://fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x:javascript-key=qdEVaFuYe08mtL81o45DhhS4wYZHlnKbLrTk2nAM@api.parse.com/1/


***Get all bands ********************************************************************
https://APPID:javascript-key=JSKEY@api.parse.com/1/classes/Bands

{"results":[{"BandName":"Gray Shack","YearFounded":{"__type":"Date","iso":"2008-01-01T21:08:00.000Z"},"Genre":{"__type":"Pointer","className":"Genre","objectId"
:"qDOEvp1iyM"},"Label":{"__type":"Pointer","className":"Labels","objectId":"U1GIlAhnto"},"createdAt":"2014-01-25T20:49:03.057Z","updatedAt":"2014-01-25T21:08:48
.573Z","objectId":"gXfsfG2S3m"}]}

***Get all songs for a band (Gray Shack)********************************************************************
curl -X GET \
  -H "X-Parse-Application-Id: fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x" \
  -H "X-Parse-REST-API-Key: vKZV2i5IKhs96Ph5lLJLCBKPtaAMGxsfIoqPduhh" \
  -G \
  --data-urlencode 'where={"Band":{"__type":"Pointer","className":"Bands","objectId":"qDOEvp1iyM"}}' \
  https://api.parse.com/1/classes/Songs

{"results":[{"Duration":"3:29","SongName":"Monkey Man","Band":{"__type":"Pointer","className":"Bands","objectId":"gXfsfG2S3m"},"createdAt":"2014-01-25T21:09:29.
501Z","updatedAt":"2014-01-29T00:21:33.883Z","objectId":"JnRZH8gGTE"},{"Duration":"3.00","SongName":"Dirty City","Band":{"__type":"Pointer","className":"Bands",
"objectId":"gXfsfG2S3m"},"createdAt":"2014-01-29T00:22:03.430Z","updatedAt":"2014-01-29T00:22:20.411Z","objectId":"wSp1FMYRV6"}]}



***Get all shows for a band (Gray Shack)********************************************************************
curl -X GET \
  -H "X-Parse-Application-Id: fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x" \
  -H "X-Parse-REST-API-Key: vKZV2i5IKhs96Ph5lLJLCBKPtaAMGxsfIoqPduhh" \
  -G \
  --data-urlencode 'where={"Band":{"__type":"Pointer","className":"Bands","objectId":"qDOEvp1iyM"}}' \
  https://api.parse.com/1/classes/Shows

{"results":[{"Date":{"__type":"Date","iso":"2014-01-25T21:12:00.000Z"},"Venue":"Arco Arena, Sacramento","Band":{"__type":"Pointer","className":"Bands","objectId
":"gXfsfG2S3m"},"createdAt":"2014-01-25T21:12:44.789Z","updatedAt":"2014-01-25T21:13:27.352Z","objectId":"Mbo31uENTw"}]}


***Get a band by objectId ********************************************************************
https://APPID:javascript-key=JSKEY@api.parse.com/1/classes/Bands/<objectId>

{"results":[{"BandName":"Gray Shack","YearFounded":{"__type":"Date","iso":"2008-01-01T21:08:00.000Z"},"Genre":{"__type":"Pointer","className":"Genre","objectId"
:"qDOEvp1iyM"},"Label":{"__type":"Pointer","className":"Labels","objectId":"U1GIlAhnto"},"createdAt":"2014-01-25T20:49:03.057Z","updatedAt":"2014-01-25T21:08:48
.573Z","objectId":"gXfsfG2S3m"}]}

***Get a song by objectId ********************************************************************
https://APPID:javascript-key=JSKEY@api.parse.com/1/classes/Songs/<objectId>


***Get a show by objectId ********************************************************************
https://APPID:javascript-key=JSKEY@api.parse.com/1/classes/Shows/<objectId>





