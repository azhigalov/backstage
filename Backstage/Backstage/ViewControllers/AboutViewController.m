//
//  AboutViewController.m
//  Backstage
//
//  Created by Anton Zhigalov on 19.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "AboutViewController.h"
#import "UIViewController+HeaderAligning.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];

    [self addNavigationTitleViewWithText:NSLocalizedString(@"Backstage", @"")];
    
    [self addRightButton];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (0 == section)
        return 117; //like in the design
    
    return 16; //like in the design
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"about-cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    if (0 == indexPath.section)
        cell.textLabel.text = [NSLocalizedString(@"Visit us online", @"") uppercaseString];
    else
        cell.textLabel.text = [NSLocalizedString(@"Contact us", @"") uppercaseString];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (0 == indexPath.section)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:WEBSITE_URL]];
    else
        [self shareVieEmail];
}

#pragma mark - MFMailComposeController delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Helpers
- (void)addRightButton {
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"dance_sing_silhouette"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 8, self.navigationController.navigationBar.frame.size.height);
    
    rightButton.contentEdgeInsets = (UIEdgeInsets){.right=-14}; //Move it to the right
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)shareVieEmail {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        [mailer setToRecipients:[NSArray arrayWithObject:EMAIL]];
        mailer.mailComposeDelegate = self;
        
        [self presentViewController:mailer animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                        message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles: nil];
        [alert show];
    }
}
@end
