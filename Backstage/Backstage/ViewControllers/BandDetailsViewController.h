//
//  BandDetailsViewController.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SongsLoader.h"
#import "ShowsLoader.h"
#import "ShowsView.h"

@class Band;

@interface BandDetailsViewController : UIViewController
<SongsLoaderDelegate, ShowsLoaderDelegate, ShowsViewDelegate>{
    SongsLoader     *songsLoader;
    ShowsLoader     *showsLoader;
    
    NSArray         *arraySongs;
    NSArray         *arrayShows;
}

@property (strong, nonatomic) Band                  *band;
@property (strong, nonatomic) UIScrollView          *contentScrollView;

@end
