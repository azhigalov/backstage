//
//  AboutViewController.h
//  Backstage
//
//  Created by Anton Zhigalov on 19.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface AboutViewController : UITableViewController
<MFMailComposeViewControllerDelegate>

@end
