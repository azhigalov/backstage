//
//  BandDetailsViewController.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "ShowDetailsViewController.h"
#import "BandView.h"
#import "ShowDetailsView.h"
#import "Show.h"
#import "UIViewController+HeaderAligning.h"

#define kBandViewHeight             150

@interface ShowDetailsViewController ()

@end

@implementation ShowDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    //resize background image to size of device(remove when fittable images will be able):
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"details_bgd"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    [self addNavigationTitleViewWithText:[NSLocalizedString(@"Show", @"") uppercaseString]];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self loadBandAndShowInfo];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Helpers
- (void)loadBandAndShowInfo {
    BandView *bandView = [[BandView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBandViewHeight)
                                                    band:self.band
                                                delegate:nil
                                              bandNamePos:BandNamePositionHidden];
    [self.view addSubview:bandView];
    
    ShowDetailsView *showDetails = [[ShowDetailsView alloc] initWithShow:self.show band:self.band];
    showDetails.frame = CGRectMake(0, 0, self.view.frame.size.width, showDetails.frame.size.height);
    self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                            kBandViewHeight,
                                                                            self.view.frame.size.width,
                                                                            self.view.frame.size.height - kBandViewHeight - self.tabBarController.tabBar.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height)];

    [self.contentScrollView addSubview:showDetails];
    
    self.contentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, showDetails.frame.origin.y + showDetails.frame.size.height);

    [self.view addSubview:self.contentScrollView];
}

@end
