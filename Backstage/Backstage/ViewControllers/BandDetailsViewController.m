//
//  BandDetailsViewController.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "BandDetailsViewController.h"
#import "BandView.h"
#import "BandDetailsView.h"
#import "SongsView.h"
#import "Song.h"
#import "Show.h"
#import "SVProgressHUD.h"
#import "Band.h"
#import "ShowDetailsViewController.h"
#import "UIViewController+HeaderAligning.h"

#define kBandViewHeight             150
#define kFailedLoadingSongsAlertTag 1
#define kFailedLoadingShowsAlertTag 2

@interface BandDetailsViewController ()

@end

@implementation BandDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    //resize background image to size of device(remove when fittable images will be able):
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"details_bgd"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    [self addNavigationTitleViewWithText:[NSLocalizedString(@"Band", @"") uppercaseString]];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    songsLoader = [[SongsLoader alloc] init];
    songsLoader.delegate = self;
    showsLoader = [[ShowsLoader alloc] init];
    showsLoader.delegate = self;
    
    [self loadBandInfo];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopPlaying" object:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toShowDetails"]) {
		ShowDetailsViewController *showDetailsViewController = segue.destinationViewController;
        showDetailsViewController.show = (Show *)sender;
        showDetailsViewController.band = self.band;
    }
}

#pragma mark - UIAlertViewDelegate methods
-(void)alertView:(UIAlertView*)aView clickedButtonAtIndex:(NSInteger)anIndex {
    if (kFailedLoadingSongsAlertTag == aView.tag)
        [self loadSongs];
    else if (kFailedLoadingShowsAlertTag == aView.tag)
        [self loadShows];
}

#pragma mark - SongsLoaderDelegate implementation
- (void)didSucceededLoadingSong:(Song *)song{}

- (void)didSucceededLoadingSongs:(NSArray *)songsArray {
    arraySongs = songsArray;
    
    [SVProgressHUD dismiss];
    
    if ([songsArray count] > 0) {
        SongsView *songsView = [[SongsView alloc] initWithSongs:arraySongs];
        
        CGRect songsViewFrame = songsView.frame;
        songsViewFrame.origin.y = self.contentScrollView.contentSize.height;
        songsView.frame = songsViewFrame;
        
        [self.contentScrollView addSubview:songsView];
        self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.contentSize.width, self.contentScrollView.contentSize.height + songsViewFrame.size.height);
    }
    
    [self loadShows];
}

- (void)didFailedLoadingSongs:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed", @"")];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:error.localizedDescription
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Try again", @"")
                                          otherButtonTitles:nil];
    alert.tag = kFailedLoadingSongsAlertTag;
    [alert show];
}

#pragma mark - ShowsLoaderDelegate implementation
- (void)didSucceededLoadingShow:(Show *)show {}

- (void)didSucceededLoadingShows:(NSArray *)showsArray {
    arrayShows = showsArray;
    [SVProgressHUD dismiss];
    
    if ([showsArray count] > 0) {
        ShowsView *showsView = [[ShowsView alloc] initWithShows:arrayShows delegate:self];
        
        CGRect showsViewFrame = showsView.frame;
        showsViewFrame.origin.y = self.contentScrollView.contentSize.height;
        showsView.frame = showsViewFrame;
        
        [self.contentScrollView addSubview:showsView];
        self.contentScrollView.contentSize = CGSizeMake(self.view.frame.size.width,
                                                        showsViewFrame.origin.y + showsViewFrame.size.height + 20);
    } else
        self.contentScrollView.contentSize = CGSizeMake(self.view.frame.size.width,
                                                        self.contentScrollView.contentSize.height + 20);
}

- (void)didFailedLoadingShows:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed", @"")];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:error.localizedDescription
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Try again", @"")
                                          otherButtonTitles:nil];
    alert.tag = kFailedLoadingShowsAlertTag;
    [alert show];
}

#pragma mark - ShowsViewDelegate implementation
- (void)showDidTap:(Show *)show {
    [self performSegueWithIdentifier:@"toShowDetails" sender:show];
}

#pragma mark - Helpers
- (void)loadBandInfo {
    BandView *bandView = [[BandView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBandViewHeight)
                                                    band:self.band
                                                delegate:nil
                                              bandNamePos:BandNamePositionDown];
    [self.view addSubview:bandView];
    
    BandDetailsView *bandDetails = [[BandDetailsView alloc] initWithBand:self.band];
    bandDetails.frame = CGRectMake(0, 0, self.view.frame.size.width, bandDetails.frame.size.height);
    
    self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                            kBandViewHeight,
                                                                            self.view.frame.size.width,
                                                                            self.view.frame.size.height - kBandViewHeight - self.tabBarController.tabBar.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height)];

    [self.contentScrollView addSubview:bandDetails];
    
    self.contentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, bandDetails.frame.origin.y + bandDetails.frame.size.height);

    [self.view addSubview:self.contentScrollView];
    
    [self loadSongs];
}

- (void)loadSongs {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading songs", @"")];
    
    [songsLoader loadSongsForBand:self.band];
}

- (void)loadShows {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading shows", @"")];
    
    [showsLoader loadShowsForBand:self.band];
}

@end
