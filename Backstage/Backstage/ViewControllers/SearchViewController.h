//
//  SearchViewController.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BandsLoader.h"

@interface SearchViewController : UITableViewController
<BandsLoaderDelegate, UISearchBarDelegate> {
    BandsLoader     *loader;
}

@end
