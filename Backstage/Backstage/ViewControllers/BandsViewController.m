//
//  BandsViewController_iPhone.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "BandsViewController.h"
#import "Band.h"
#import "SVProgressHUD.h"
#import "BandDetailsViewController.h"
#import "UIViewController+HeaderAligning.h"

#define kBandViewHeight         150
#define kFailedLoadingAlertTag  1

@interface BandsViewController ()

@end

@implementation BandsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self addNavigationTitleViewWithText:[NSLocalizedString(@"For you", @"") uppercaseString]];
    
    loader = [[BandsLoader alloc] init];
    loader.delegate = self;
    
    [self addRightButton];
    [self loadBands];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toBandDetails"]) {
		BandDetailsViewController *bandDetailsViewController = segue.destinationViewController;
        bandDetailsViewController.band = ((BandView *)sender).band;
    }
}

#pragma mark - BandViewDelegate
- (void)bandTapped:(BandView *)bandView {
    [self performSegueWithIdentifier:@"toBandDetails" sender:bandView];
}

#pragma mark - UIAlertViewDelegate methods
-(void)alertView:(UIAlertView*)aView clickedButtonAtIndex:(NSInteger)anIndex {
    if (kFailedLoadingAlertTag == aView.tag)
        [self loadBands];
}

#pragma mark - BandsLoaderDelegate
- (void)didSucceededLoadingBand:(Band *)band {}

- (void)didSucceededLoadingBands:(NSArray *)bandsArray {
    int i = 0;
    bands = [[NSMutableArray alloc] initWithArray:bandsArray];
    
    for (Band *band in bandsArray) {
        BandView *bandView = [[BandView alloc] initWithFrame:CGRectMake(0, i * kBandViewHeight, self.view.frame.size.width, kBandViewHeight)
                                                        band:band
                                                    delegate:self
                                                  bandNamePos:BandNamePositionCenter];
        
        [self.scrollView addSubview:bandView];
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, (i + 1) * kBandViewHeight)];
        
        i++;
    }
    
    [SVProgressHUD dismiss];
}

- (void)didFailedLoadingBands:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed", @"")];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:error.localizedDescription
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Try again", @"")
                                          otherButtonTitles:nil];
    alert.tag = kFailedLoadingAlertTag;
    [alert show];
}

#pragma mark - Helpers
- (void)loadBands {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", @"")];
    
    [loader loadAll];
}

- (void)addRightButton {
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"dance_sing_silhouette"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 8, self.navigationController.navigationBar.frame.size.height);
    
    rightButton.contentEdgeInsets = (UIEdgeInsets){.right=-14}; //Move it to the right
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}
@end
