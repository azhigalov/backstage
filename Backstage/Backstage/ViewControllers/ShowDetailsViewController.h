//
//  BandDetailsViewController.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Show;
@class Band;

@interface ShowDetailsViewController : UIViewController{
}

@property (strong, nonatomic) Show                  *show;
@property (strong, nonatomic) Band                  *band;
@property (strong, nonatomic) UIScrollView          *contentScrollView;

@end
