//
//  SearchViewController.m
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "SearchViewController.h"
#import "Defines.h"
#import "FoundPlaceView.h"
#import "SVProgressHUD.h"
#import "SearchResultsViewController.h"
#import "UIViewController+HeaderAligning.h"

@interface SearchViewController () {
    UISearchBar *bandSearchBar;
}

@end

@implementation SearchViewController

- (void)viewDidLoad{
    [super viewDidLoad];

    [self addNavigationTitleViewWithText:[NSLocalizedString(@"Discover", @"") uppercaseString]];
    
    self.tableView.backgroundColor = TABLE_BACKGROUND_COLOR;
    
    loader = [[BandsLoader alloc] init];
    loader.delegate = self;
    
    [self addRightButton];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (0 == indexPath.section) {
        if (0 == indexPath.row) {
            static NSString *cellIdentifier = @"search-cell";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                
                CGRect searchBarFrame = CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, cell.contentView.bounds.size.height);
                bandSearchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
                bandSearchBar.placeholder = [NSLocalizedString(@"Search by location", @"") uppercaseString];
                bandSearchBar.delegate = self;
                bandSearchBar.barTintColor = SEARCH_BAR_TINT_COLOR;
                
                //hide the border
                bandSearchBar.layer.borderWidth = 1;
                bandSearchBar.layer.borderColor = [[UIColor whiteColor] CGColor];

                //Custom font for the search bar
                for(int i = 0; i < [bandSearchBar.subviews count]; i++)
                    if([[bandSearchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
                        [(UITextField*)[bandSearchBar.subviews objectAtIndex:i] setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
                
                [cell.contentView addSubview:bandSearchBar];
            }
            
            return cell;
        } else if (1 == indexPath.row) {
            static NSString *cellIdentifier = @"music-now-cell";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                cell.backgroundColor = CELL_BACKGROUND_COLOR_RED;
                cell.imageView.image = [UIImage imageNamed:@"music_sign_white"];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"white_accessory"]];
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
            }
            
            cell.textLabel.text = [NSLocalizedString(@"Music Now!", @"") uppercaseString];
            
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (1 == indexPath.row)
        [self loadBandsForZipcode:bandSearchBar.text]; //95630 - is a working code
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toSearchResults"]) {
		SearchResultsViewController *searchResultsViewController = segue.destinationViewController;
        searchResultsViewController.bands = (NSArray *)sender;
    }
}

#pragma mark - UISearchBarDelegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self loadBandsForZipcode:searchBar.text];//95630 - is a working code
}

#pragma mark - BandsLoaderDelegate
- (void)didSucceededLoadingBand:(Band *)band {}

- (void)didSucceededLoadingBands:(NSArray *)bandsArray {
    if ([bandsArray count] > 0) {
        [SVProgressHUD dismiss];
        
        [self performSegueWithIdentifier:@"toSearchResults" sender:bandsArray];
    } else
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"No data found", @"")];
}

- (void)didFailedLoadingBands:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed", @"")];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:error.localizedDescription
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Helpers
- (void)addRightButton {
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"dance_sing_silhouette"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 8, self.navigationController.navigationBar.frame.size.height);
    
    rightButton.contentEdgeInsets = (UIEdgeInsets){.right=-14}; //Move it to the right
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)loadBandsForZipcode:(NSString *)zipcode {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Searching", @"")];
    
    if ([zipcode length] == 0)
        [loader loadAll];
    else
        [loader loadForZipcode:zipcode];
}
@end
