//
//  SearchResultsViewController.h
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UITableViewController

@property (strong, nonatomic) NSArray   *bands;

@end
