//
//  BandsViewController_iPhone.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BandView.h"
#import "BandsLoader.h"

@interface BandsViewController : UIViewController
<BandViewDelegate, BandsLoaderDelegate> {
    BandsLoader     *loader;
    NSMutableArray  *bands;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
