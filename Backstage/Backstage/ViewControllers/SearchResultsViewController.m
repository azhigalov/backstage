//
//  SearchResultsViewController.m
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "SearchResultsViewController.h"
#import "TableCellSearchResult.h"
#import "Band.h"
#import "NSDate+Additions.h"
#import "UIImageView+AFNetworking.h"
#import "BandDetailsViewController.h"
#import "UIViewController+HeaderAligning.h"

@interface SearchResultsViewController ()

@end

@implementation SearchResultsViewController

- (void)viewDidLoad{
    [super viewDidLoad];

    [self addNavigationTitleViewWithText:[NSLocalizedString(@"Discover", @"") uppercaseString]];
    
    [self addRightButton];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toBandDetails"]) {
		BandDetailsViewController *bandDetailsViewController = segue.destinationViewController;
        bandDetailsViewController.band = (Band *)sender;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return [self.bands count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"searchResultsCell";
    Band *band = [self.bands objectAtIndex:indexPath.row];
    
    TableCellSearchResult *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.labelName.text = band.name;
    cell.labelDate.text = [band.yearFounded dateToStringWithDateFormat:@"MM/dd/yy"];
    cell.labelPrice.text = band.origin;
    [cell.imageViewPhoto setImageWithURL:[NSURL URLWithString:band.thumbnailUrl]
                        placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Band *band = [self.bands objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"toBandDetails" sender:band];
}

#pragma mark - Helpers
- (void)addRightButton {
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"dance_sing_silhouette"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 8, self.navigationController.navigationBar.frame.size.height);
    
    rightButton.contentEdgeInsets = (UIEdgeInsets){.right=-14}; //Move it to the right
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}

@end
