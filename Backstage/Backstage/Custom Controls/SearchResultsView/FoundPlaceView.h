//
//  SearchResultsView.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PlaceRoundedCornersTypeNone = 0,
	PlaceRoundedCornersTypeTop = 1,
    PlaceRoundedCornersTypeBottom = 2,
    PlaceRoundedCornersTypeBoth = 3
} PlaceRoundedCornersType;

@interface FoundPlaceView : UIView

- (id)initWithFrame:(CGRect)frame text:(NSString *)text cornersType:(PlaceRoundedCornersType)cornersType;

@end
