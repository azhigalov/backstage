//
//  SearchResultsView.m
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "FoundPlaceView.h"
#import <QuartzCore/QuartzCore.h>

#define kViewCornerRadius   4

@implementation FoundPlaceView

- (id)initWithFrame:(CGRect)frame text:(NSString *)text cornersType:(PlaceRoundedCornersType)cornersType {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.clipsToBounds = YES;
        
        if (cornersType == PlaceRoundedCornersTypeNone) {
            self.layer.borderColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1].CGColor;
            self.layer.borderWidth = 0.5f;
        } else {
            UIRectCorner corners;
            
            if (cornersType == PlaceRoundedCornersTypeTop)
                corners = UIRectCornerTopLeft | UIRectCornerTopRight;
            else if (cornersType == PlaceRoundedCornersTypeBottom)
                corners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            else
                corners = UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight;
            
            CGSize radii = CGSizeMake(kViewCornerRadius, kViewCornerRadius);
            
            UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                       byRoundingCorners:corners
                                                             cornerRadii:radii];
            
            // Mask the container view’s layer to round the corners.
            CAShapeLayer *cornerMaskLayer = [CAShapeLayer layer];
            [cornerMaskLayer setPath:path.CGPath];
            self.layer.mask = cornerMaskLayer;
            
            // Make a transparent, stroked layer which will dispay the stroke.
            CAShapeLayer *strokeLayer = [CAShapeLayer layer];
            strokeLayer.path = path.CGPath;
            strokeLayer.fillColor = [UIColor clearColor].CGColor;
            strokeLayer.strokeColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1].CGColor;
            strokeLayer.lineWidth = 1; // the stroke splits the width evenly inside and outside,
            // but the outside part will be clipped by the containerView’s mask.
            
            // Transparent view that will contain the stroke layer
            UIView *strokeView = [[UIView alloc] initWithFrame:self.bounds];
            strokeView.userInteractionEnabled = NO; // in case your container view contains controls
            [strokeView.layer addSublayer:strokeLayer];
            
            // configure and add any subviews to the container view
            
            // stroke view goes in last, above all the subviews
            [self addSubview:strokeView];
        }

        UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [text uppercaseString];
        
        [self addSubview:label];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
