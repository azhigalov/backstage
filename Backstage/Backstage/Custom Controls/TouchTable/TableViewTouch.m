#import "TableViewTouch.h"
#import "Defines.h"

@interface TableViewTouch() {
    BOOL isTouched;
}
@end

@implementation TableViewTouch

@synthesize tableDelegate;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];       

    isTouched = YES;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    isTouched = NO;
}

- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[super touchesEnded:touches withEvent:event];
    
    if (isTouched)
        [self.tableDelegate tableTapped];
}

@end
