#import <Foundation/Foundation.h>

@protocol TableViewTouchDelegate;

@interface TableViewTouch : UITableView {
    BOOL isMoved;
}

@property (nonatomic, weak)  id<TableViewTouchDelegate>	tableDelegate;

@end


@protocol TableViewTouchDelegate
- (void)tableTapped;

@end