//
//  BandDetailsView.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "BandDetailsView.h"
#import "Band.h"
#import "NSDate+Additions.h"
#import "Pointer.h"

@implementation BandDetailsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithBand:(Band *)band {
    self = [[[NSBundle mainBundle] loadNibNamed:@"BandDetailsView" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        self.labelFounded.text = [NSString stringWithFormat:@":  %@", [band.yearFounded dateToStringWithDateFormat:@"yyyy"]];
        self.labelGenre.text = [NSString stringWithFormat:@":  %@", band.genrePointer.name];
        self.labelLabels.text = [NSString stringWithFormat:@":  %@", band.labelPointer.name];
        self.labelOrigin.text = [NSString stringWithFormat:@":  %@", band.origin];
    }
    
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.labelBandDetails.attributedText = [[NSAttributedString alloc] initWithString:[NSLocalizedString(@"Band details", @"") uppercaseString]
                                                                           attributes:underlineAttribute];
}

@end
