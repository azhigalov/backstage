//
//  BandDetailsView.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Band;

@interface BandDetailsView : UIView 

@property (weak, nonatomic) IBOutlet UILabel *labelBandDetails;
@property (weak, nonatomic) IBOutlet UILabel *labelOrigin;
@property (weak, nonatomic) IBOutlet UILabel *labelGenre;
@property (weak, nonatomic) IBOutlet UILabel *labelFounded;
@property (weak, nonatomic) IBOutlet UILabel *labelLabels;

- (id)initWithBand:(Band *)band;

@end
