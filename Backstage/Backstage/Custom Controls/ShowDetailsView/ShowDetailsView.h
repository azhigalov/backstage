//
//  BandDetailsView.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Show;
@class Band;

@interface ShowDetailsView : UIView

@property (weak, nonatomic) IBOutlet UILabel *labelShowDetails;
@property (weak, nonatomic) IBOutlet UILabel *labelBandName;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;

- (id)initWithShow:(Show *)show band:(Band *)band;

@end
