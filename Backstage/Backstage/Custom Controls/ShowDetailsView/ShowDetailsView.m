//
//  BandDetailsView.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "ShowDetailsView.h"
#import "Show.h"
#import "NSDate+Additions.h"
#import "Pointer.h"
#import "Band.h"

@implementation ShowDetailsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithShow:(Show *)show band:(Band *)band {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ShowDetailsView" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        self.labelBandName.text = [NSString stringWithFormat:@":  %@", band.name];
        self.labelDate.text = [NSString stringWithFormat:@":  %@", [show.date dateToStringWithDateFormat:nil]];
        self.labelAddress.text = [NSString stringWithFormat:@":  %@", show.venue];
    }
    
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.labelShowDetails.attributedText = [[NSAttributedString alloc] initWithString:[NSLocalizedString(@"Show details", @"") uppercaseString]
                                                                           attributes:underlineAttribute];
}

@end
