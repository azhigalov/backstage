//
//  ISMPProgressHUD.h
//  ISMP
//
//  Created by Anton Zhigalov on 17.02.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import "MBProgressHUD.h"

@protocol ISMPProgressHUDDelegate;

@interface ISMPProgressHUD : MBProgressHUD<MBProgressHUDDelegate> {
    id<ISMPProgressHUDDelegate> ismpDelegate;
}

@property (assign) int comletedIndicatorDuration;

- (ISMPProgressHUD *)initForKeyWindowWithDelegate:(id<ISMPProgressHUDDelegate>)delegate;
- (void)showDimTextIndicator:(NSString *)text;
- (void)showCompletedIndicator:(NSString *)text;
- (void)showTextIndicator:(NSString *)text;
- (void)showFailedIndicator:(NSString *)text;
- (void)hide:(BOOL)animated;
@end

@protocol ISMPProgressHUDDelegate
- (void)indicatorDidHide:(ISMPProgressHUD *)hudIndicator;
@end