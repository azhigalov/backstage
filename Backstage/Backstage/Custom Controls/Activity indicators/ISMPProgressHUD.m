//
//  ISMPProgressHUD.m
//  ISMP
//
//  Created by Anton Zhigalov on 17.02.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import "ISMPProgressHUD.h"

@implementation ISMPProgressHUD

@synthesize comletedIndicatorDuration = _comletedIndicatorDuration;

#pragma mark - MBProgressHUDDelegate methods
- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self removeFromSuperview];
    [ismpDelegate indicatorDidHide:self];
}

#pragma mark - Public functions
- (ISMPProgressHUD *)initForKeyWindowWithDelegate:(id<ISMPProgressHUDDelegate>)delegate {
	UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    self = [super initWithFrame:keyWindow.bounds];
    
    if (self) {
        self.delegate = self;
        ismpDelegate = delegate;
        self.comletedIndicatorDuration = 1;
        self.opacity = 0.65;
    }
	
	return self;
}

- (void)showTextIndicator:(NSString *)text {
    //Perform block after delay. In case i need to show the indicator after an alertView
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    
        self.mode = MBProgressHUDModeIndeterminate;
        self.labelText = text;
        
        [self show:YES];
    });
}

- (void)showDimTextIndicator:(NSString *)text {
    //Perform block after delay. In case i need to show the indicator after an alertView
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];
        
        self.dimBackground = YES;
        self.labelText = text;
        
        [self show:YES];
    });
}

- (void)showCompletedIndicator:(NSString *)text {
    //Perform block after delay. In case i need to show the indicator after an alertView
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];;
        
        UIImageView *checkmarkView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
        self.customView = checkmarkView;
        self.mode = MBProgressHUDModeCustomView;
        self.labelText = text;

        [self show:YES];
        [self hide:YES afterDelay:self.comletedIndicatorDuration];
    });
}

- (void)showFailedIndicator:(NSString *)text {
    //Perform block after delay. In case i need to show the indicator after an alertView
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
        [[[UIApplication sharedApplication] keyWindow] addSubview:self];;
        
        UIImageView *checkmarkView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"failure.png"]];
        self.customView = checkmarkView;
        self.mode = MBProgressHUDModeCustomView;
        self.labelText = text;
        
        [self show:YES];
        [self hide:YES afterDelay:self.comletedIndicatorDuration];
    });
}

- (void)hide:(BOOL)animated {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [super hide:animated];
    });
}
@end
