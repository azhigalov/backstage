//
//  SongView.h
//  Backstage
//
//  Created by Anton Zhigalov on 06.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShowViewDelegate;

@class Show;

@interface ShowView : UIView

@property (strong, nonatomic) UILabel   *labelShowName;
@property (strong, nonatomic) UILabel   *labelShowDate;
@property (strong, nonatomic) Show      *show;

@property (nonatomic, weak) id<ShowViewDelegate>    delegate;

- (id)initWithShow:(Show *)show delegate:(id<ShowViewDelegate>)delegate;

@end

@protocol ShowViewDelegate <NSObject>

- (void)showViewDidTap:(ShowView *)showView;

@end