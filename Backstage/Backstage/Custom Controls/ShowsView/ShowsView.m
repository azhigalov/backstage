//
//  BandDetailsView.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "ShowsView.h"
#import "Show.h"

#define kFirstShowY 40

@implementation ShowsView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithShows:(NSArray *)shows delegate:(id<ShowsViewDelegate>)delegate {
    self = [super initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 0)];
    
    if (self) {
        self.delegate = delegate;
        int i = 0;
        
        self.labelShows = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, [[UIScreen mainScreen] bounds].size.width, 21)];
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                             NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:15],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]};
        self.labelShows.textAlignment = NSTextAlignmentCenter;
        
        self.labelShows.attributedText = [[NSAttributedString alloc] initWithString:[NSLocalizedString(@"Shows", @"") uppercaseString]
                                                                         attributes:underlineAttribute];
        
        [self addSubview:self.labelShows];
        
        for (Show *show in shows) {
            ShowView *showView = [[ShowView alloc] initWithShow:show delegate:self];
            
            CGRect showViewFrame = showView.frame;
            showViewFrame.origin.y = showViewFrame.size.height * i + kFirstShowY + 20; //20 is margin between songs
            showView.frame = showViewFrame;
            
            CGRect selfFrame = self.frame;
            selfFrame.size.height = showViewFrame.origin.y + showViewFrame.size.height;
            self.frame = selfFrame;
            
            [self addSubview:showView];
            
            i++;
        }
    }

    return self;
}

#pragma mark - ShowViewDelegate methods
- (void)showViewDidTap:(ShowView *)showView {
    [self.delegate showDidTap:showView.show];
}
@end
