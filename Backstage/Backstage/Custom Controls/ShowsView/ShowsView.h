//
//  BandDetailsView.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowView.h"

@protocol ShowsViewDelegate;

@interface ShowsView : UIView<ShowViewDelegate>

@property (strong, nonatomic) UILabel *labelShows;

- (id)initWithShows:(NSArray *)shows delegate:(id<ShowsViewDelegate>)delegate; //Array of Show objects

@property (nonatomic, weak) id<ShowsViewDelegate>    delegate;

@end

@protocol ShowsViewDelegate <NSObject>

- (void)showDidTap:(Show *)show;

@end