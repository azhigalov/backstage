//
//  SongView.m
//  Backstage
//
//  Created by Anton Zhigalov on 06.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "ShowView.h"
#import "Show.h"
#import "NSDate+Additions.h"

#define kViewHeight             19
#define kShowNameLabelWidth     180
#define kShowDateLabelWidth     100

@implementation ShowView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (id)initWithShow:(Show *)show delegate:(id<ShowViewDelegate>)delegate {
    self = [super initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, kViewHeight)];
    
    if (self) {
        self.show = show;
        self.delegate = delegate;
        
        self.labelShowName = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, kShowNameLabelWidth, kViewHeight)];
        self.labelShowName.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        self.labelShowName.textColor = [UIColor whiteColor];
        self.labelShowName.textAlignment = NSTextAlignmentLeft;
        self.labelShowName.text = show.venue;
        
        [self addSubview:self.labelShowName];
        
        self.labelShowDate = [[UILabel alloc] initWithFrame:CGRectMake(20 + kShowNameLabelWidth, 0, kShowDateLabelWidth, kViewHeight)];
        self.labelShowDate.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        self.labelShowDate.textColor = [UIColor whiteColor];
        self.labelShowDate.textAlignment = NSTextAlignmentRight;
        self.labelShowDate.text = [show.date dateToStringWithDateFormat:nil];
        
        [self addSubview:self.labelShowDate];
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        singleTapGestureRecognizer.numberOfTapsRequired = 1;
        [self addGestureRecognizer:singleTapGestureRecognizer];
    }
    
    return self;
}

#pragma mark - Actions
- (IBAction)tap:(id)sender {
    [self.delegate showViewDidTap:self];
}
@end
