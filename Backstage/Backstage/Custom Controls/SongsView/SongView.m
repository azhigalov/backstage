//
//  SongView.m
//  Backstage
//
//  Created by Anton Zhigalov on 06.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "SongView.h"
#import "Song.h"
#import <AVFoundation/AVFoundation.h>
#import "Utilities.h"

#define kViewHeight             30
#define kSongNameLabelWidth     161
#define kSongDurationLabelWidth 80
#define kButtonPlayHeight       26

@interface SongView () {
}

@property (strong, nonatomic) AVPlayer    *player;
@property (strong, nonatomic) NSTimer     *playbackTimer;
@end

@implementation SongView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"stopPlaying" object:nil];
}

- (id)initWithSong:(Song *)song delegate:(id<SongViewDelegate>)delegate {
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kViewHeight)];
    
    if (self) {
        self.song = song;
        self.delegate = delegate;
        
        self.buttonPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        self.buttonPlay.frame = CGRectMake(20, (kViewHeight - kButtonPlayHeight) / 2, kButtonPlayHeight, kButtonPlayHeight);
        [self.buttonPlay setBackgroundImage:[UIImage imageNamed:@"white_play"] forState:UIControlStateNormal];
        [self.buttonPlay setBackgroundImage:[UIImage imageNamed:@"red_play"] forState:UIControlStateSelected];
        [self.buttonPlay addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.buttonPlay];
        
        self.labelSongName = [[UILabel alloc] initWithFrame:CGRectMake(20 + kViewHeight, 0, kSongNameLabelWidth, kViewHeight)]; //20 is the margin between the button and label
        self.labelSongName.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        self.labelSongName.textColor = [UIColor whiteColor];
        self.labelSongName.textAlignment = NSTextAlignmentLeft;
        self.labelSongName.text = song.name;
        
        [self addSubview:self.labelSongName];
        
        self.labelSongDuration = [[UILabel alloc] initWithFrame:CGRectMake(20 + kViewHeight + kSongNameLabelWidth, 0, kSongDurationLabelWidth, kViewHeight)];
        self.labelSongDuration.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        self.labelSongDuration.textColor = [UIColor whiteColor];
        self.labelSongDuration.textAlignment = NSTextAlignmentRight;
        self.labelSongDuration.text = song.duration;
        
        [self addSubview:self.labelSongDuration];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlaying" object:nil];
    }
    
    return self;
}

#pragma mark - Property accessors
- (BOOL)isPlaying {
    return (self.player != nil);
}

#pragma mark - Actions
- (IBAction)play:(id)sender {
    if (self.buttonPlay.selected) {
        if (self.player.status == AVPlayerStatusReadyToPlay)
            [self stopPlayer];
    } else
        [self startPlayer];
}

#pragma mark - Audio playback observing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.player && [keyPath isEqualToString:@"status"]) {
        if (self.player.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
            [self.delegate songDidFailPlaying:self.song];
            
            self.buttonPlay.selected = NO;
        } else if (self.player.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            
            [self.delegate songDidStartPlaying:self.song];

            self.buttonPlay.selected = YES;
            
            self.playbackTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
        } else if (self.player.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
            //Maybe do somethung later
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self stopPlayer];
    
    [self.delegate songDidEndPlaying:self.song];
}

#pragma mark - Helpers
- (void)stopPlayer {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.player currentItem]];
    [self.player removeObserver:self forKeyPath:@"status"];
    
    self.player = nil;
    
    [self.playbackTimer invalidate];
    self.playbackTimer = nil;
    
    self.buttonPlay.selected = NO;
    self.labelSongDuration.text = self.song.duration;
}

- (void)startPlayer {
    NSURL *songUrl = [NSURL URLWithString:self.song.songUrl];
    NSError *error;
    
    if (songUrl) {
        self.player = [AVPlayer playerWithURL:songUrl];
        
        if (error) {
            NSLog(@"Failed to play the song: %@", error.localizedDescription);
            [self stopPlayer];
            
            return;
        }
        
        [self.delegate songDidStartPreparingToPlay:self.song];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[self.player currentItem]];
        [self.player addObserver:self forKeyPath:@"status" options:0 context:nil];

        [self.player play];
    } else
        NSLog(@"Failed to compose the url");
}

- (void)updateProgress {
    NSTimeInterval interval = self.player.currentTime.value/self.player.currentTime.timescale;
    
    if (interval == 0)
        self.labelSongDuration.text = NSLocalizedString(@"Buffering...", @"");
    else
        self.labelSongDuration.text = [Utilities formattedStrTimeForInterval:self.player.currentTime.value/self.player.currentTime.timescale];
}
@end
