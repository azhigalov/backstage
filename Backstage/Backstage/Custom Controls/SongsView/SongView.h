//
//  SongView.h
//  Backstage
//
//  Created by Anton Zhigalov on 06.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SongViewDelegate;

@class Song;

@interface SongView : UIView

@property (strong, nonatomic) UIButton              *buttonPlay;
@property (strong, nonatomic) UILabel               *labelSongName;
@property (strong, nonatomic) UILabel               *labelSongDuration;

@property (strong, nonatomic) Song                  *song;
@property (nonatomic, weak) id<SongViewDelegate>    delegate;
@property (nonatomic, assign, readonly) BOOL        isPlaying;

- (id)initWithSong:(Song *)song delegate:(id<SongViewDelegate>)delegate;
- (void)startPlayer;
- (void)stopPlayer;
@end

@protocol SongViewDelegate <NSObject>

- (void)songDidStartPreparingToPlay:(Song *)song;
- (void)songDidStartPlaying:(Song *)song;
- (void)songDidEndPlaying:(Song *)song;
- (void)songDidFailPlaying:(Song *)song;

@end