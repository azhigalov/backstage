//
//  BandDetailsView.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SongView.h"

@interface SongsView : UIView
<SongViewDelegate>

@property (strong, nonatomic) UILabel *labelSongs;

- (id)initWithSongs:(NSArray *)songs; //Array of Song objects
@end
