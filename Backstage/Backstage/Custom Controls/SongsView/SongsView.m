//
//  BandDetailsView.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "SongsView.h"
#import "Song.h"
#import "SVProgressHUD.h"

#define kFirstSongY 40

@implementation SongsView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithSongs:(NSArray *)songs {
    self = [super initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 0)];
    
    if (self) {
        self.userInteractionEnabled = YES;
        
        int i = 0;
        
        self.labelSongs = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, [[UIScreen mainScreen] bounds].size.width, 21)];
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                             NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:15],
                                             NSForegroundColorAttributeName: [UIColor whiteColor]};
        self.labelSongs.textAlignment = NSTextAlignmentCenter;
        
        self.labelSongs.attributedText = [[NSAttributedString alloc] initWithString:[NSLocalizedString(@"Songs", @"") uppercaseString]
                                                                         attributes:underlineAttribute];
        
        [self addSubview:self.labelSongs];
        
        for (Song *song in songs) {
            SongView *songView = [[SongView alloc] initWithSong:song delegate:self];
            
            CGRect songViewFrame = songView.frame;
            songViewFrame.origin.y = songViewFrame.size.height * i + kFirstSongY + 20;
            songView.frame = songViewFrame;
            
            CGRect selfFrame = self.frame;
            selfFrame.size.height = songViewFrame.origin.y + songViewFrame.size.height;
            self.frame = selfFrame;
            
            [self addSubview:songView];
            
            i++;
        }
    }

    return self;
}

#pragma mark - SongViewDelegate methods
- (void)songDidStartPreparingToPlay:(Song *)song {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Preparing to play", @"")];
}

- (void)songDidStartPlaying:(Song *)song {
    [SVProgressHUD dismiss];

    [self stopCurrentPlayingSongIfNeeded:song];
}

- (void)songDidEndPlaying:(Song *)song {
    [SVProgressHUD dismiss];
}

- (void)songDidFailPlaying:(Song *)song {
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed", @"")];
}

#pragma mark - Helpers
- (void)stopCurrentPlayingSongIfNeeded:(Song *)songToPlay {
    for (UIView *subView in self.subviews)
        if ([subView isMemberOfClass:[SongView class]]) {
            SongView *songView = (SongView *)subView;
            
            if (songView.song != songToPlay)
                if (songView.isPlaying)
                    [songView stopPlayer];
        }
}
@end
