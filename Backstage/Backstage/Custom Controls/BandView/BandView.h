//
//  BandView.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    BandNamePositionCenter = 1,
    BandNamePositionDown,
    BandNamePositionHidden
    
} BandNamePosition;

@protocol BandViewDelegate;

@class Band;

@interface BandView : UIView

@property (weak, nonatomic) NSObject<BandViewDelegate>  *delegate;

@property (strong, nonatomic) UIImageView               *imageViewBandThumbnail;
@property (strong, nonatomic) UILabel                   *labelBandName;
@property (strong, nonatomic) Band                      *band;

- (id)initWithFrame:(CGRect)frame
               band:(Band *)band
           delegate:(NSObject<BandViewDelegate> *)delegate
         bandNamePos:(BandNamePosition)bandNamePos;
@end

@protocol BandViewDelegate <NSObject>

- (void)bandTapped:(BandView *)bandView;

@end