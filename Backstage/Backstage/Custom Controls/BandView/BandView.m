//
//  BandView.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "BandView.h"
#import "Band.h"
#import <QuartzCore/QuartzCore.h>

//#define kTestImageUrl   @"http://habr.habrastorage.org/post_images/399/cd1/9b6/399cd19b625b445ef56cc07bf7298340.jpg"

@implementation BandView

- (id)initWithFrame:(CGRect)frame
               band:(Band *)band
           delegate:(NSObject<BandViewDelegate> *)delegate
        bandNamePos:(BandNamePosition)bandNamePos {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = delegate;
        self.band = band;
        self.clipsToBounds = YES;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = self.bounds;
        [button addTarget:self action:@selector(tap:) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"highlight_bgd"] forState:UIControlStateHighlighted];
        
        self.imageViewBandThumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, self.bounds.size.width, self.bounds.size.height - 1)]; //1px as a separator
        self.imageViewBandThumbnail.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewBandThumbnail.clipsToBounds = YES;
        
        [self addSubview:self.imageViewBandThumbnail];
       
        self.labelBandName = [[UILabel alloc] init];
        
        if (bandNamePos != BandNamePositionHidden)
            self.labelBandName.frame = bandNamePos == BandNamePositionDown ? CGRectMake(0, frame.size.height - 30, frame.size.width, 30) : self.bounds;
        else
            self.labelBandName.hidden = YES;
        
        self.labelBandName.textColor = [UIColor whiteColor];
        self.labelBandName.textAlignment = NSTextAlignmentCenter;
        self.labelBandName.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        self.labelBandName.text = [band.name uppercaseString];
        
        [self addSubview:self.labelBandName];
        
        [self addSubview:button];
        
        if (band.thumbnailImage)
            self.imageViewBandThumbnail.image = band.thumbnailImage;
        else {
            //If the band url exists
            [self loadImage];
        }
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Actions
- (IBAction)tap:(id)sender {
    [self.delegate bandTapped:self];
}

#pragma mark - Helpers
- (void)loadImage {
    dispatch_queue_t requestQueue = dispatch_queue_create("load thumb in bgd", NULL);
    
    UIActivityIndicatorView   *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    [indicator startAnimating];
    
    [self addSubview:indicator];
    
    dispatch_async(requestQueue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_band.thumbnailUrl]];
        UIImage *thumbImage = nil;
        
        if (imageData)
            thumbImage = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [indicator removeFromSuperview];
            
            if (thumbImage) {
                _imageViewBandThumbnail.image = thumbImage;
                _band.thumbnailImage = thumbImage;
                
                CATransition *transition = [CATransition animation];
                transition.duration = 0.75f;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionFade;
                
                [_imageViewBandThumbnail.layer addAnimation:transition forKey:nil];
            }
        });
    });
}
@end
