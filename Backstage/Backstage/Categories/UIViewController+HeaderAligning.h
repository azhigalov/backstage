//
//  UIViewController+HeaderAligning.h
//  Backstage
//
//  Created by Anton Zhigalov on 18.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HeaderAligning)

- (void)addNavigationTitleViewWithText:(NSString *)text;
@end
