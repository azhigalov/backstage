//
//  UIAlertView+Additions.m
//  ISMP
//
//  Created by Anton Zhigalov on 13.05.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import "UIAlertView+Additions.h"
#import <objc/runtime.h>

NSString * const kUIAlertViewUserInfoKey = @"kUIAlertViewUserInfoKey";

@implementation UIAlertView (Additions)

@dynamic userInfo;

- (NSDictionary *)userInfo {
	return objc_getAssociatedObject(self, (__bridge const void *)(kUIAlertViewUserInfoKey));
}

- (void)setUserInfo:(NSDictionary *)userInfo {
    objc_setAssociatedObject(self, (__bridge const void *)(kUIAlertViewUserInfoKey), userInfo, OBJC_ASSOCIATION_RETAIN);
}


@end
