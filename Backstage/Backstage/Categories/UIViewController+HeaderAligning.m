//
//  UIViewController+HeaderAligning.m
//  Backstage
//
//  Created by Anton Zhigalov on 18.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "UIViewController+HeaderAligning.h"

@implementation UIViewController (HeaderAligning)

- (void)addNavigationTitleViewWithText:(NSString *)text {
    // Init views with rects with height and y pos
    CGFloat titleHeight = self.navigationController.navigationBar.frame.size.height;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectZero];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.text = text;
    
    // Set font for sizing width
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    CGSize origDetailsSize = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName:titleLabel.font}];
    
    CGRect labelRect = [titleLabel.text boundingRectWithSize:origDetailsSize
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:titleLabel.font}
                                                     context:nil];
    CGRect frame;
    
    frame = titleLabel.frame;
    frame.size.height = titleHeight;
    frame.size.width = labelRect.size.width + 0.5;
    titleLabel.frame = frame;
    
    frame = titleView.frame;
    frame.size.height = titleHeight;
    frame.size.width = labelRect.size.width;
    titleView.frame = frame;
    
    // Ensure text is on one line, centered and truncates if the bounds are restricted
    titleLabel.numberOfLines = 1;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    // Use autoresizing to restrict the bounds to the area that the titleview allows
    titleView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    titleView.autoresizesSubviews = YES;
    titleLabel.autoresizingMask = titleView.autoresizingMask;
    
    // Add as the nav bar's titleview
    [titleView addSubview:titleLabel];
    
    self.navigationItem.titleView = titleView;
}

@end
