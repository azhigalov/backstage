//
//  UIAlertView+Additions.h
//  ISMP
//
//  Created by Anton Zhigalov on 13.05.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Additions)

@property(nonatomic, retain) NSDictionary *userInfo;
@end
