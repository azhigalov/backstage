//
//  NSDate+Additions.h
//  Tele FM
//
//  Created by Anton Zhigalov on 19.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)
- (NSString *)getDurationUntilDate:(NSDate *)endTime;
- (NSString *)getBriefDurationUntilDate:(NSDate *)endTime;
- (NSString *)getDateAndIntervalUntilDate:(NSDate *)endTime;
- (NSString *)getDayOfWeek;
- (NSString *)getTime;
- (NSString *)dateToStringWithDateFormat:(NSString *)dateFormat;
@end
