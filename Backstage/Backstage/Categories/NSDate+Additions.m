//
//  NSDate+Additions.m
//  Tele FM
//
//  Created by Anton Zhigalov on 19.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)

- (NSString *)getDurationUntilDate:(NSDate *)endTime {
    if (nil == endTime)
        return @"";
    
    NSTimeInterval timeInSeconds = [endTime timeIntervalSinceDate:self];
    double mins = timeInSeconds / 60;
    double hours = mins / 60;
    
    NSMutableString *duration = [[NSMutableString alloc] init];
    if (hours>=1)
        [duration appendFormat:NSLocalizedString(@"%0.f hour(s) ", @""), floor(hours)];
    if (mins>=1)
        [duration appendFormat:NSLocalizedString(@"%0.f mins", @""), mins - floor(hours)*60];
    
    return duration;
}

- (NSString *)getBriefDurationUntilDate:(NSDate *)endTime {
    if (nil == endTime)
        return @"";
    
    NSTimeInterval timeInSeconds = [endTime timeIntervalSinceDate:self];
    double mins = timeInSeconds / 60;
    double hours = mins / 60;
    
    NSMutableString *duration = [[NSMutableString alloc] init];
    if (hours>=1)
        [duration appendFormat:NSLocalizedString(@"%0.fh", @""), floor(hours)];
    if (mins>=1)
        [duration appendFormat:NSLocalizedString(@" %0.fm", @""), mins - floor(hours)*60];
    
    return duration;
}

- (NSString *)getDateAndIntervalUntilDate:(NSDate *)endTime {
    if (nil == endTime)
        return @"";
    
    NSMutableString *result = [[NSMutableString alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [result appendString:[formatter stringFromDate:self]];
    
    [formatter setDateStyle:NSDateFormatterNoStyle];
    
    [result appendFormat:@"-%@", [formatter stringFromDate:endTime]];
	
    return result;
}

- (NSString *)getDayOfWeek {
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EEEE" options:0 locale:[NSLocale currentLocale]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formatString];
    
    return [formatter stringFromDate:self];
}

- (NSString *)getTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    return [formatter stringFromDate:self];
}

- (NSString *)dateToStringWithDateFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
	
    if (dateFormat)
        [formatter setDateFormat:dateFormat];
    else {
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
    }
    
    return [formatter stringFromDate:self];
}

@end
