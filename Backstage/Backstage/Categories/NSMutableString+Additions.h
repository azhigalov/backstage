//
//  NSMutableString+Additions.h
//  ISMP
//
//  Created by Anton Zhigalov on 09.10.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (Additions)
- (NSMutableString *)xmlSimpleUnescape;
- (NSMutableString *)xmlSimpleEscape;
@end
