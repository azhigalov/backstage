//
//  UIActionSheet+Additions.h
//  ISMP
//
//  Created by Anton Zhigalov on 22.08.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (Additions)

@property(nonatomic, retain) NSObject *userInfo;
@end
