//
//  UIActionSheet+Additions.m
//  ISMP
//
//  Created by Anton Zhigalov on 22.08.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import "UIActionSheet+Additions.h"
#import <objc/runtime.h>

NSString * const kUIActionSheetUserInfoKey = @"kUIActionSheetUserInfoKey";

@implementation UIActionSheet (Additions)

@dynamic userInfo;

- (NSObject *)userInfo {
	return objc_getAssociatedObject(self, (__bridge const void *)(kUIActionSheetUserInfoKey));
}

- (void)setUserInfo:(NSObject *)userInfo {
    objc_setAssociatedObject(self, (__bridge const void *)(kUIActionSheetUserInfoKey), userInfo, OBJC_ASSOCIATION_RETAIN);
}

@end