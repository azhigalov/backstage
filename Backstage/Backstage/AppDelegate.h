//
//  AppDelegate.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
