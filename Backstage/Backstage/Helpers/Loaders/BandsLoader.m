//
//  BandsLoader.m
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "BandsLoader.h"
#import "BandParser.h"

@implementation BandsLoader

- (id)init {
    if (self = [super init]) {
        request = [[NSMutableURLRequest alloc] init];
        request.timeoutInterval = 10;
    }
    return self;
}

#pragma mark - Public methods
- (void)loadAll {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", PARSE_URL_COMMON, PARSE_BANDS_ALL];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingBands];
}

- (void)loadWithId:(NSString *)identifier {
    NSString *methodUrlPart = [NSString stringWithFormat:PARSE_BAND_FORMAT, identifier];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", PARSE_URL_COMMON, methodUrlPart];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingBand];
}
- (void)loadForZipcode:(NSString *)zipcode {
    NSString *requestString = [[BandParser getJSONRequestForZipcode:zipcode] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSString *urlString = [NSString stringWithFormat:@"%@%@?where=%@", PARSE_URL_COMMON, PARSE_BANDS_ALL, requestString];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingBands];
}

#pragma mark - Private methods
- (void)startLoadingBands {
    __block id<BandsLoaderDelegate> blockDelegate = self.delegate;
    
    requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"response (bands): %@", [NSString stringWithUTF8String:[responseObject bytes]]);
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error == nil) {
            NSArray *resultArray = [response objectForKey:@"results"];
            NSMutableArray *bandsArray = [NSMutableArray array];
            
            for (NSDictionary *dict in resultArray) {
                Band *newBand = [BandParser getBandFromDictionary:dict];
                [bandsArray addObject:newBand];
            }
            
            [blockDelegate didSucceededLoadingBands:[NSArray arrayWithArray:bandsArray]];
        } else {
            NSLog(@"Error(loading bands): %@", error.localizedDescription);
            
            [blockDelegate didFailedLoadingBands:error];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error(loading bands): %@", error.localizedDescription);
        
        [blockDelegate didFailedLoadingBands:error];
    }];
    
    [requestOperation start];
}

- (void)startLoadingBand {
    __block id<BandsLoaderDelegate> blockDelegate = self.delegate;
    
    requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"response (bands): %@", [NSString stringWithUTF8String:[responseObject bytes]]);
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error == nil) {
            Band *newBand = [BandParser getBandFromDictionary:response];
            [blockDelegate didSucceededLoadingBand:newBand];
        } else {
            NSLog(@"Error(loading bands): %@", error.localizedDescription);
            
            [blockDelegate didFailedLoadingBands:error];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error(loading bands): %@", error.localizedDescription);
        
        [blockDelegate didFailedLoadingBands:error];
    }];
    
    [requestOperation start];
}

@end
