//
//  SongsLoader.m
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "SongsLoader.h"
#import "SongParser.h"
#import "Band.h"
#import "PointerParser.h"
#import "BandParser.h"

@implementation SongsLoader

- (id)init {
    if (self = [super init]) {
        request = [[NSMutableURLRequest alloc] init];
        request.timeoutInterval = 10;
    }
    return self;
}

#pragma mark - Public methods
- (void)loadAll {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", PARSE_URL_COMMON, PARSE_SONGS_ALL];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingSongs];
}

- (void)loadWithId:(NSString *)identifier {
    NSString *methodUrlPart = [NSString stringWithFormat:PARSE_SONG_FORMAT, identifier];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", PARSE_URL_COMMON, methodUrlPart];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingSong];
}

- (void)loadSongsForBand:(Band *)band {
    NSString *requestString = [[BandParser getJSONRequestForBand:band] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSString *urlString = [NSString stringWithFormat:@"%@%@?where=%@", PARSE_URL_COMMON, PARSE_SONGS_ALL, requestString];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingSongs];
}

#pragma mark - Private methods
- (void)startLoadingSongs {
    __block id<SongsLoaderDelegate> blockDelegate = self.delegate;
    
    requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"response (songs): %@", [NSString stringWithUTF8String:[responseObject bytes]]);
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error == nil) {
            NSArray *resultArray = [response objectForKey:@"results"];
            NSMutableArray *songsArray = [NSMutableArray array];
            
            for (NSDictionary *dict in resultArray) {
                Song *newSong = [SongParser getSongFromDictionary:dict];
                [songsArray addObject:newSong];
            }
            
            [blockDelegate didSucceededLoadingSongs:[NSArray arrayWithArray:songsArray]];
        } else {
            NSLog(@"Error(loading songs): %@", error.localizedDescription);
            
            [blockDelegate didFailedLoadingSongs:error];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error(loading songs): %@", error.localizedDescription);
        
        [blockDelegate didFailedLoadingSongs:error];
    }];
    
    [requestOperation start];
}

- (void)startLoadingSong {
    __block id<SongsLoaderDelegate> blockDelegate = self.delegate;
    
    requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"response (songs): %@", [NSString stringWithUTF8String:[responseObject bytes]]);
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error == nil) {
            Song *newSong = [SongParser getSongFromDictionary:response];
            [blockDelegate didSucceededLoadingSong:newSong];
        } else {
            NSLog(@"Error(loading songs): %@", error.localizedDescription);
            
            [blockDelegate didFailedLoadingSongs:error];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error(loading songs): %@", error.localizedDescription);
        
        [blockDelegate didFailedLoadingSongs:error];
    }];
    
    [requestOperation start];
}


@end
