//
//  SongsLoader.h
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "AFNetworking.h"

@class Band;
@protocol SongsLoaderDelegate;

@class Song;

@interface SongsLoader : NSObject {
    AFHTTPRequestOperation      *requestOperation;
    NSMutableURLRequest         *request;
}

@property (nonatomic, weak) id<SongsLoaderDelegate>  delegate;

- (void)loadAll;
- (void)loadWithId:(NSString *)identifier;
- (void)loadSongsForBand:(Band *)band;

@end


@protocol SongsLoaderDelegate <NSObject>

- (void)didSucceededLoadingSong:(Song *)song;
- (void)didSucceededLoadingSongs:(NSArray *)songsArray;
- (void)didFailedLoadingSongs:(NSError *)error;

@end
