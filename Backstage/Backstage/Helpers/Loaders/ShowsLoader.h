//
//  ShowsLoader.h
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "AFNetworking.h"

@class Band;
@protocol ShowsLoaderDelegate;

@class Show;

@interface ShowsLoader : NSObject {
    AFHTTPRequestOperation      *requestOperation;
    NSMutableURLRequest         *request;
}

@property (nonatomic, weak) id<ShowsLoaderDelegate>  delegate;

- (void)loadAll;
- (void)loadWithId:(NSString *)identifier;
- (void)loadShowsForBand:(Band *)band;

@end


@protocol ShowsLoaderDelegate <NSObject>

- (void)didSucceededLoadingShow:(Show *)show;
- (void)didSucceededLoadingShows:(NSArray *)showsArray;
- (void)didFailedLoadingShows:(NSError *)error;

@end
