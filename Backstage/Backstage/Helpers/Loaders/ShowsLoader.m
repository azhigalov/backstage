//
//  ShowsLoader.m
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "ShowsLoader.h"
#import "ShowParser.h"
#import "Band.h"
#import "PointerParser.h"
#import "BandParser.h"

@implementation ShowsLoader

- (id)init {
    if (self = [super init]) {
        request = [[NSMutableURLRequest alloc] init];
        request.timeoutInterval = 10;
    }
    return self;
}

#pragma mark - Public methods
- (void)loadAll {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", PARSE_URL_COMMON, PARSE_SHOWS_ALL];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingShows];
}

- (void)loadWithId:(NSString *)identifier {
    NSString *methodUrlPart = [NSString stringWithFormat:PARSE_SHOW_FORMAT, identifier];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", PARSE_URL_COMMON, methodUrlPart];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingShow];
}

- (void)loadShowsForBand:(Band *)band {
    NSString *requestString = [[BandParser getJSONRequestForBand:band] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSString *urlString = [NSString stringWithFormat:@"%@%@?where=%@", PARSE_URL_COMMON, PARSE_SHOWS_ALL, requestString];
    
    NSURL *url = [NSURL URLWithString:urlString];
    [request setURL:url];
    
    [request setValue:PARSE_APPID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_RESTAPIKEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [self startLoadingShows];
}

#pragma mark - Private methods
- (void)startLoadingShows {
    __block id<ShowsLoaderDelegate> blockDelegate = self.delegate;
    
    requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"response (shows): %@", [NSString stringWithUTF8String:[responseObject bytes]]);
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error == nil) {
            NSArray *resultArray = [response objectForKey:@"results"];
            NSMutableArray *showsArray = [NSMutableArray array];
            
            for (NSDictionary *dict in resultArray) {
                Show *newShow = [ShowParser getShowFromDictionary:dict];
                [showsArray addObject:newShow];
            }
            
            [blockDelegate didSucceededLoadingShows:[NSArray arrayWithArray:showsArray]];
        } else {
            NSLog(@"Error(loading shows): %@", error.localizedDescription);
            
            [blockDelegate didFailedLoadingShows:error];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error(loading shows): %@", error.localizedDescription);
        
        [blockDelegate didFailedLoadingShows:error];
    }];
    
    [requestOperation start];
}

- (void)startLoadingShow {
    __block id<ShowsLoaderDelegate> blockDelegate = self.delegate;
    
    requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"response (shows): %@", [NSString stringWithUTF8String:[responseObject bytes]]);
        
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        if (error == nil) {
            Show *newShow = [ShowParser getShowFromDictionary:response];
            [blockDelegate didSucceededLoadingShow:newShow];
        } else {
            NSLog(@"Error(loading shows): %@", error.localizedDescription);
            
            [blockDelegate didFailedLoadingShows:error];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error(loading shows): %@", error.localizedDescription);
        
        [blockDelegate didFailedLoadingShows:error];
    }];
    
    [requestOperation start];
}

@end
