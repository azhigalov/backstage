//
//  BandsLoader.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "AFNetworking.h"

@protocol BandsLoaderDelegate;

@class Band;

@interface BandsLoader : NSObject {
    AFHTTPRequestOperation      *requestOperation;
    NSMutableURLRequest         *request;
}

@property (nonatomic, weak) id<BandsLoaderDelegate>  delegate;

- (void)loadAll;
- (void)loadWithId:(NSString *)identifier;
- (void)loadForZipcode:(NSString *)zipcode;

@end


@protocol BandsLoaderDelegate <NSObject>

- (void)didSucceededLoadingBand:(Band *)band;
- (void)didSucceededLoadingBands:(NSArray *)bandsArray;
- (void)didFailedLoadingBands:(NSError *)error;

@end