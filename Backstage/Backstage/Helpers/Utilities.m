//
//  Utilities.m
//  ISMP
//
//  Created by Anton Zhigalov on 08.04.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (NSDate *)stringToDate:(NSString *)strDate format:(NSString *)dateFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
	[formatter setDateFormat:dateFormat];
    
    return [formatter dateFromString:strDate];
}

+ (NSString *)formattedStrTimeForInterval:(NSInteger)interval {
    int seconds;
    int minutes;
    int hours;
    
    seconds = interval % 60;
    minutes = (interval / 60) % 60;
    hours = (int)(interval / 3600);
    
    NSString *timeInfoString = nil;
	if (hours > 0)
		timeInfoString = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
	else
		timeInfoString = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    
    return timeInfoString;
}

+ (NSString *)formattedPrice:(float)price {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    return price >= 0 ?
            [formatter stringFromNumber:[NSNumber numberWithFloat:price]] :
            [NSString stringWithFormat:@"-%@", [formatter stringFromNumber:[NSNumber numberWithFloat:-price]]];;
}

+ (NSString *)getUUID{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    NSString *resultString = (__bridge NSString *)string;
    CFRelease(theUUID);
    CFRelease(string);
    return resultString;
}

+ (NSString *)get20CharsUUID {
    NSString *uuid = [Utilities getUUID];
    NSString *uuidNoDash = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    if ([uuidNoDash length] > 20)
        return [uuidNoDash substringToIndex:20];
    
    return uuidNoDash;
}

+ (void)createDirectoryInDocumentsFolderWithName:(NSString *)directoryName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *yourDirPath = [documentsDirectory stringByAppendingPathComponent:directoryName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir = YES;
    BOOL isDirExists = [fileManager fileExistsAtPath:yourDirPath isDirectory:&isDir];
    if (!isDirExists)
        [fileManager createDirectoryAtPath:yourDirPath withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (void)saveFile:(NSData *)fileData withName:(NSString *)fileName intoDirectory:(NSString *)directoryName {
    [self createDirectoryInDocumentsFolderWithName:directoryName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@/%@", directoryName, fileName]];
    
    BOOL isDir = NO;
    BOOL isFileExists = [fileManager fileExistsAtPath:filePath isDirectory:&isDir];
    if (isFileExists) {
        [fileData writeToFile:filePath atomically:YES];
    } else {
        [fileManager createFileAtPath:filePath contents:fileData attributes:nil];
    }
}

+ (NSData *)loadDataFromFile:(NSString *)fileName atDirectory:(NSString *)directoryName {
    NSData *fileData = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@/%@", directoryName, fileName]];

    BOOL isDir = NO;
    BOOL isFileExists = [fileManager fileExistsAtPath:filePath isDirectory:&isDir];
    if (isFileExists) {
        fileData = [[NSData alloc] initWithContentsOfFile:filePath];
    }
    return fileData;
}

+ (void)deleteFile:(NSString *)fileName atDirectory:(NSString *)directoryName {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@/%@", directoryName, fileName]];
    
    BOOL isDir = NO;
    BOOL isFileExists = [fileManager fileExistsAtPath:filePath isDirectory:&isDir];
    if (isFileExists) {
        NSError *error = nil;
        [fileManager removeItemAtPath:filePath error:&error];
    }
}

//It just checks the length
+ (BOOL)isMacAddress:(NSString *)address {
    return [[address stringByReplacingOccurrencesOfString:@":" withString:@""] length] == 12;
}

@end
