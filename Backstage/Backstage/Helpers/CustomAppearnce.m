//
//  CustomAppearnce.m
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "CustomAppearnce.h"
#import "Defines.h"

@implementation CustomAppearnce

+ (void)customizeUITabBarAppearance {
    [[UITabBar appearance] setTintColor:TAB_BAR_ITEM_SEL_COLOR];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
}

+ (void)customizeUINavBarAppearance {
    [[UINavigationBar appearance] setTintColor:NAV_BAR_TINT_COLOR];
}
@end
