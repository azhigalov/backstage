//
//  Utilities.h
//  ISMP
//
//  Created by Anton Zhigalov on 08.04.13.
//  Copyright (c) 2013 OpenSolution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

+ (NSDate *)stringToDate:(NSString *)strDate format:(NSString *)dateFormat;
+ (NSString *)formattedStrTimeForInterval:(NSInteger)interval;
+ (NSString *)formattedPrice:(float)price;
+ (NSString *)getUUID;
+ (NSString *)get20CharsUUID;
+ (void)createDirectoryInDocumentsFolderWithName:(NSString *)directoryName;
+ (void)saveFile:(NSData *)fileData withName:(NSString *)fileName intoDirectory:(NSString *)directoryName;
+ (NSData *)loadDataFromFile:(NSString *)fileName atDirectory:(NSString *)directoryName;
+ (void)deleteFile:(NSString *)fileName atDirectory:(NSString *)directoryName;
+ (BOOL)isMacAddress:(NSString *)address;
@end
