//
//  CustomAppearnce.h
//  Backstage
//
//  Created by Anton Zhigalov on 03.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomAppearnce : NSObject

+ (void)customizeUITabBarAppearance;
+ (void)customizeUINavBarAppearance;

@end
