//
//  Pointer.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#define POINTER_TYPE        @"Pointer"

@interface Pointer : NSObject {
    
}

@property (nonatomic, strong) NSString      *identifier;
@property (nonatomic, strong) NSString      *name;

@end
