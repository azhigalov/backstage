//
//  Song.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

@class Pointer;

@interface Song : NSObject {

}

@property (nonatomic, strong) NSString      *identifier;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *duration;
@property (nonatomic, strong) Pointer       *bandPointer;
@property (nonatomic, strong) NSDate        *createdAt;
@property (nonatomic, strong) NSDate        *updatedAt;
@property (nonatomic, strong) NSString      *songUrl;

@end
