//
//  Show.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

@class Pointer;

@interface Show : NSObject {

}

@property (nonatomic, strong) NSString      *identifier;
@property (nonatomic, strong) NSString      *venue;
@property (nonatomic, strong) NSDate        *date;
@property (nonatomic, strong) Pointer       *bandPointer;
@property (nonatomic, strong) NSDate        *createdAt;
@property (nonatomic, strong) NSDate        *updatedAt;

@end
