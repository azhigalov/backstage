//
//  Band.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

@class Pointer;

@interface Band : NSObject {
    
}

- (Pointer *)pointer;

@property (nonatomic, strong) NSString      *identifier;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *origin;
@property (nonatomic, strong) NSString      *thumbnailUrl;
@property (nonatomic, strong) NSDate        *yearFounded;
@property (nonatomic, strong) Pointer       *genrePointer;
@property (nonatomic, strong) Pointer       *labelPointer;
@property (nonatomic, strong) NSDate        *createdAt;
@property (nonatomic, strong) NSDate        *updatedAt;
@property (nonatomic, strong) UIImage       *thumbnailImage;

@end
