//
//  Band.m
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "Band.h"
#import "Pointer.h"

#define POINTER_CLASS_NAME      @"Bands"

@implementation Band

- (Pointer *)pointer {
    Pointer *pointer = [[Pointer alloc] init];
    
    pointer.name = POINTER_CLASS_NAME;
    pointer.identifier = self.identifier;
    
    return pointer;
}

@end
