//
//  ShowParser.m
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "ShowParser.h"
#import "PointerParser.h"

#define SHOW_PARSER_DATE_FORMAT     @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzz'Z'"

@implementation ShowParser

+ (Show *)getShowFromDictionary:(NSDictionary *)JSON {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SHOW_PARSER_DATE_FORMAT];
    
    Show *show = [[Show alloc] init];
    
    show.identifier = VALID_STRING_FROM_JSON([JSON objectForKey:@"objectId"]);
    show.venue = VALID_STRING_FROM_JSON([JSON objectForKey:@"Venue"]);
    
    NSDictionary *dateDictionary = [JSON objectForKey:@"Date"];
    if (dateDictionary) {
        NSString *dateString = VALID_STRING_FROM_JSON([dateDictionary objectForKey:@"iso"]);
        show.date = dateString ? [dateFormatter dateFromString:dateString] : nil;
    }
    
    show.bandPointer = [PointerParser getPointerFromDictionary:[JSON objectForKey:@"Band"]];
    
    NSString *createdAtString = VALID_STRING_FROM_JSON([JSON objectForKey:@"createdAt"]);
    show.createdAt = createdAtString ? [dateFormatter dateFromString:createdAtString] : nil;
    
    NSString *updatedAtString = VALID_STRING_FROM_JSON([JSON objectForKey:@"updatedAt"]);
    show.updatedAt = updatedAtString ? [dateFormatter dateFromString:updatedAtString] : nil;
    
    return show;
}

@end
