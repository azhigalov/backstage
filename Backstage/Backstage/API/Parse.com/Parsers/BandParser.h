//
//  BandParser.h
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "Band.h"

@interface BandParser : NSObject

+ (Band *)getBandFromDictionary:(NSDictionary *)JSON;
+ (NSString *)getJSONRequestForBand:(Band *)band;
+ (NSString *)getJSONRequestForZipcode:(NSString *)zipcode;

@end
