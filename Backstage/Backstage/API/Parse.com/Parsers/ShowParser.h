//
//  ShowParser.h
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "Show.h"

@interface ShowParser : NSObject

+ (Show *)getShowFromDictionary:(NSDictionary *)JSON;

@end
