//
//  SongParser.m
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "SongParser.h"
#import "PointerParser.h"

#define SONG_PARSER_DATE_FORMAT     @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzz'Z'"

@implementation SongParser

+ (Song *)getSongFromDictionary:(NSDictionary *)JSON {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SONG_PARSER_DATE_FORMAT];
    
    Song *song = [[Song alloc] init];
    
    song.identifier = VALID_STRING_FROM_JSON([JSON objectForKey:@"objectId"]);
    song.name = VALID_STRING_FROM_JSON([JSON objectForKey:@"SongName"]);
    song.duration = VALID_STRING_FROM_JSON([JSON objectForKey:@"Duration"]);
    song.songUrl = VALID_STRING_FROM_JSON([JSON objectForKey:@"SongUrl"]);
    
    song.bandPointer = [PointerParser getPointerFromDictionary:[JSON objectForKey:@"Band"]];
    
    NSString *createdAtString = VALID_STRING_FROM_JSON([JSON objectForKey:@"createdAt"]);
    song.createdAt = createdAtString ? [dateFormatter dateFromString:createdAtString] : nil;
    
    NSString *updatedAtString = VALID_STRING_FROM_JSON([JSON objectForKey:@"updatedAt"]);
    song.updatedAt = updatedAtString ? [dateFormatter dateFromString:updatedAtString] : nil;
    
    return song;
}

@end
