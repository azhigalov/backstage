//
//  PointerParser.m
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "PointerParser.h"

@implementation PointerParser

+ (Pointer *)getPointerFromDictionary:(NSDictionary *)JSON {
    Pointer *pointer = [[Pointer alloc] init];
    
    pointer.identifier = VALID_STRING_FROM_JSON([JSON objectForKey:@"objectId"]);
    pointer.name = VALID_STRING_FROM_JSON([JSON objectForKey:@"className"]);
    
    return pointer;
}

+ (NSDictionary *)getDictionaryFromPointer:(Pointer *)pointer {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    [dictionary setObject:POINTER_TYPE forKey:@"__type"];
    [dictionary setObject:pointer.name forKey:@"className"];
    [dictionary setObject:pointer.identifier forKey:@"objectId"];
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}


@end
