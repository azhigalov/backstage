//
//  BandParser.m
//  Backstage
//
//  Created by Anton Zhigalov on 04.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "BandParser.h"
#import "PointerParser.h"

#define BAND_PARSER_DATE_FORMAT     @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzz'Z'"
#define kTestImageUrl               @"http://habr.habrastorage.org/post_images/399/cd1/9b6/399cd19b625b445ef56cc07bf7298340.jpg"

@implementation BandParser

+ (Band *)getBandFromDictionary:(NSDictionary *)JSON {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:BAND_PARSER_DATE_FORMAT];
    
    Band *band = [[Band alloc] init];
    
    band.identifier = VALID_STRING_FROM_JSON([JSON objectForKey:@"objectId"]);
    band.name = VALID_STRING_FROM_JSON([JSON objectForKey:@"BandName"]);
    band.origin = VALID_STRING_FROM_JSON([JSON objectForKey:@"Origin"]);
    band.thumbnailUrl = VALID_STRING_FROM_JSON([JSON objectForKey:@"BandImage"]);
    
    NSDictionary *yearFoundedDictionary = [JSON objectForKey:@"YearFounded"];
    if (yearFoundedDictionary) {
        NSString *yearFoundedString = VALID_STRING_FROM_JSON([yearFoundedDictionary objectForKey:@"iso"]);
        band.yearFounded = yearFoundedString ? [dateFormatter dateFromString:yearFoundedString] : nil;
    }
    
    band.genrePointer = [PointerParser getPointerFromDictionary:[JSON objectForKey:@"Genre"]];
    band.labelPointer = [PointerParser getPointerFromDictionary:[JSON objectForKey:@"Label"]];
    
    NSString *createdAtString = VALID_STRING_FROM_JSON([JSON objectForKey:@"createdAt"]);
    band.createdAt = createdAtString ? [dateFormatter dateFromString:createdAtString] : nil;
    
    NSString *updatedAtString = VALID_STRING_FROM_JSON([JSON objectForKey:@"updatedAt"]);
    band.updatedAt = updatedAtString ? [dateFormatter dateFromString:updatedAtString] : nil;
    
    return band;
}

+ (NSString *)getJSONRequestForBand:(Band *)band {
    NSDictionary *requestDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[PointerParser getDictionaryFromPointer:[band pointer]], @"Band", nil];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDictionary options:0 error:&error];
    NSString *json = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    return json;
}

+ (NSString *)getJSONRequestForZipcode:(NSString *)zipcode {
    NSDictionary *requestDictionary = [NSDictionary dictionaryWithObject:zipcode forKey:@"ZIP"];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDictionary options:0 error:&error];
    NSString *json = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    return json;
}
@end
