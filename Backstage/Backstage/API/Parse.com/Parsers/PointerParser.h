//
//  PointerParser.h
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "Pointer.h"

@interface PointerParser : NSObject

+ (Pointer *)getPointerFromDictionary:(NSDictionary *)JSON;
+ (NSDictionary *)getDictionaryFromPointer:(Pointer *)pointer;

@end
