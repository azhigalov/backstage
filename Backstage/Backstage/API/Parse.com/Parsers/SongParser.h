//
//  SongParser.h
//  Backstage
//
//  Created by Anton Zhigalov on 05.02.14.
//  Copyright (c) 2014 Wellframe. All rights reserved.
//

#import "Song.h"

@interface SongParser : NSObject

+ (Song *)getSongFromDictionary:(NSDictionary *)JSON;

@end
