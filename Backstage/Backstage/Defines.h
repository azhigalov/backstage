//
//  Defines.h
//
//  Created by Anton Zhigalov

#define TABLE_CELL_HEIGHT_DEFAULT       44
#define KEYBOARD_ANIMATION_DURATION		0.25
#define POPUP_ANIMATION_DURATION        0.35
#define KEYBOARD_HEIGHT                 216.0

#define RGBCOLOR(r,g,b)                 [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBACOLOR(r,g,b,a)              [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define VALID_STRING_FROM_JSON(string)  (!string || ((NSNull *)string == [NSNull null]) ? @"" : string)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_DEVICE_IPAD                              ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define HEIGHT_IPHONE_5                             568
#define IS_IPHONE_5                                 ([UIScreen mainScreen].bounds.size.height == HEIGHT_IPHONE_5)

#define TAB_BAR_ITEM_SEL_COLOR          RGBCOLOR(255, 45, 85)
#define NAV_BAR_TINT_COLOR              RGBCOLOR(0, 0, 0)
#define CELL_BACKGROUND_COLOR_RED       RGBCOLOR(255, 45, 85)
#define SEARCH_BAR_TINT_COLOR           RGBCOLOR(249, 249, 249)
#define TABLE_BACKGROUND_COLOR          RGBCOLOR(239, 239, 239)


//parse.com application keys
#define PARSE_APPID         @"fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x"
#define PARSE_JSKEY         @"qdEVaFuYe08mtL81o45DhhS4wYZHlnKbLrTk2nAM"
#define PARSE_RESTAPIKEY    @"vKZV2i5IKhs96Ph5lLJLCBKPtaAMGxsfIoqPduhh"

//parse.com URLs
#define PARSE_URL           @"https://fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x:javascript-key=qdEVaFuYe08mtL81o45DhhS4wYZHlnKbLrTk2nAM@api.parse.com/1/"
//#define PARSE_URL           @"https://PARSE_APPID:javascript-key=PARSE_JSKEY@api.parse.com/1/"
//#define PARSE_URL_REST      @"https://fA4lC3khPCylmReuXMkapW9E2GrUDHxM4NJryH6x:rest-api-key=vKZV2i5IKhs96Ph5lLJLCBKPtaAMGxsfIoqPduhh@api.parse.com/1/"
#define PARSE_URL_COMMON    @"https://api.parse.com/1/"

//parse.com API methods
#define PARSE_BANDS_ALL     @"classes/Bands"
#define PARSE_BAND_FORMAT   @"classes/Bands/%@"
#define PARSE_SHOWS_ALL     @"classes/Shows"
#define PARSE_SHOW_FORMAT   @"classes/Shows/%@"
#define PARSE_SONGS_ALL     @"classes/Songs"
#define PARSE_SONG_FORMAT   @"classes/Songs/%@"

#define WEBSITE_URL         @"http://www.takemebackstage.com"
#define EMAIL               @"info@takemebackstage.com"