//
//  TableCellWithText.h
//  Furrit
//
//  Created by Anton Zhigalov on 03.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Defines.h"


@interface TableCellSearchResult : UITableViewCell {
}
@property (weak, nonatomic) IBOutlet UIImageView    *imageViewPhoto;
@property (weak, nonatomic) IBOutlet UILabel        *labelName;
@property (weak, nonatomic) IBOutlet UILabel        *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel        *labelDate;

@end